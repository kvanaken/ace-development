import nuke
import sys
import os
import tempfile
import shutil
import subprocess
import json


def editorial_clip(render_path, source_path, base_name, first_frame, last_frame):
    nuke.toNode('Plate').knob('file').setValue(source_path)
    nuke.toNode('OUTPUT').knob('file').setValue(render_path + '/' + base_name + '.mov')
    nuke.toNode('Burn')['disable'].setValue(1)
    nuke.knob("root.first_frame", first_frame)
    nuke.knob("root.last_frame", last_frame)
    nuke.toNode('OUTPUT').knob('colorspace').setValue(3)
    nuke.execute(nuke.toNode('OUTPUT'))


def vfx_preview(render_path, source_path, version, base_name, first_frame, last_frame):
    nuke.toNode('OUTPUT').knob('file_type').setValue(3)
    nuke.toNode('Plate').knob('file').setValue(source_path)
    nuke.toNode('ApplyLut')['disable'].setValue(1)
    dirpath = tempfile.mkdtemp()
    ffmpeg_after_render_cb(dirpath, render_path, base_name, first_frame)
    nuke.toNode('OUTPUT').knob('file').setValue(dirpath + '/' + 'temp_exr.%04d.exr')
    nuke.toNode('OUTPUT').knob('colorspace').setValue(3)
    nuke.toNode('Burn')['disable'].setValue(0)
    nuke.toNode('Burn.version').knob('message').setValue(str(version))
    nuke.knob("root.first_frame", first_frame)
    nuke.knob("root.last_frame", last_frame)
    nuke.execute(nuke.toNode('OUTPUT'))


def cleanup_temp_dir(dirpath):
    shutil.rmtree(dirpath)
    nuke.toNode('OUTPUT').knob('afterRender')


def ffmpeg_after_render_cb(dirpath, render_path, base_name, first_frame):
    nuke.toNode('OUTPUT').knob('afterRender')
    nuke.addAfterRender(ffmpeg_gen, (dirpath, render_path, base_name, first_frame))


def ffmpeg_gen(dirpath, render_path, base_name, first_frame):
    lut_file = lut_lookup(base_name)
    node = nuke.toNode('Plate')
    ffmpeg_cmd = ['ffmpeg', '-s', '1920x1080', '-start_number', first_frame, '-i', 
    dirpath + '/temp_exr.%04d.exr', '-vcodec', 'libx264', '-crf', '25',
    '-timecode', '{timecode}'.format(timecode=node.metadata()['input/timecode']), '-r', '24',
    render_path + '/' + base_name + '_h264' + '.mov']
    subprocess.call(ffmpeg_cmd)
    cleanup_temp_dir(dirpath)

def lut_lookup(base_name):
    base_name_parts = base_name.split('_')
    print(base_name)
    seq = base_name_parts[1]

    with open('/home/user/Desktop/ace-development/lut_mapping.json', 'r') as f:
        lut_mapping = json.load(f)
    
    return lut_mapping[lut_mapping[seq]]


def prepare_lut(base_name):
    lut_file = lut_lookup(base_name)
    n = nuke.toNode('ApplyLut')
    n.knob('vfield_file').setValue(lut_file)
    n.knob('file_type').setValue('cube')
    n.knob('colorspaceIn').setValue('AlexaV3LogC')
    n.knob('colorspaceOut').setValue('sRGB')


if __name__ == '__main__':
    base_name = sys.argv[-1:][0]
    version = sys.argv[-2:][0]
    mov_edit_path = sys.argv[-3:][0]
    mov_preview_path = sys.argv[-4:][0]
    source_path = sys.argv[-5:][0]
    last_frame = sys.argv[-6:][0]
    first_frame = sys.argv[-7:][0]
    
    nuke.scriptSource('/mnt/projects/potemkino/hfg_2019-11-20/outgoing/delivery/dev/deliverable.nk')
    prepare_lut(base_name)
    editorial_clip(mov_edit_path, source_path, base_name, first_frame, last_frame)
    vfx_preview(mov_preview_path, source_path, version, base_name, first_frame, last_frame)
