import sys
import os
from datetime import datetime
import subprocess

BASE_PATH = r'/mnt/projects/potemkino/hfg_2019-11-20/outgoing/delivery/dev'



def run_nuke(source_path, mov_preview_path, mov_edit_path, first_frame, last_frame):
    _, file_name = os.path.split(source_path)
    base_name = file_name.split('.')[0]
    _, _, _, version = get_info_from_path(source_path)
    subprocess.call(['nuke', '-t', BASE_PATH + '/code/nuke_launch.py', first_frame, last_frame, source_path, mov_preview_path, mov_edit_path, version, base_name])


def get_info_from_path(source_path):
    _, file_name = os.path.split(source_path)
    file_name_parts = file_name.split('_')
    seq = file_name_parts[1]
    shot = file_name_parts[2]
    version = file_name_parts[4]

    return seq, shot, file_name, version


def generate_folders(source_path):
    today_date_str = datetime.today().strftime("%Y-%m-%d")
    seq, shot, file_name, _ = get_info_from_path(source_path)
    exr_path = BASE_PATH + r'/OUT/{today_date_str}/{file_name}/_exr'.format(today_date_str=today_date_str, seq=seq, shot=shot, file_name=file_name.split('.')[0])
    mov_preview_path = BASE_PATH + r'/OUT/{today_date_str}/{file_name}/_h264'.format(today_date_str=today_date_str, seq=seq, shot=shot, file_name=file_name.split('.')[0])
    mov_edit_path = BASE_PATH + r'/OUT/{today_date_str}/{file_name}/_edit'.format(today_date_str=today_date_str, seq=seq, shot=shot, file_name=file_name.split('.')[0])
    os.makedirs(exr_path)
    os.makedirs(mov_preview_path)
    os.makedirs(mov_edit_path)
    link_path = BASE_PATH + r'/OUT/{today_date_str}/{file_name}/_exr'.format(today_date_str=today_date_str, seq=seq, shot=shot, file_name=file_name.split('.')[0])
    link_file = BASE_PATH + r'/OUT/{today_date_str}/{file_name}/_exr/{file_name}'.format(today_date_str=today_date_str, seq=seq, shot=shot, file_name=file_name.split('.')[0])
    path_to_seq = os.path.relpath(os.path.split(source_path)[0], link_path)
    os.symlink(path_to_seq, link_file)
    return mov_preview_path, mov_edit_path


if __name__ == '__main__':
    source_path = sys.argv[-3:][0]
    first_frame = sys.argv[-2:][0]
    last_frame = sys.argv[-1:][0]
    
    mov_preview_path, mov_edit_path = generate_folders(source_path)
    run_nuke(source_path, mov_preview_path, mov_edit_path, first_frame, last_frame)
