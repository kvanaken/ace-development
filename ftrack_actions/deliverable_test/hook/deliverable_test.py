# Python
import logging
import subprocess
from datetime import datetime
import os
import json

os.sys.path.append('/mnt/projects/potemkino/hfg_2019-11-20/outgoing/delivery/dev/api/ftrack')
# fTrack
import ftrack_api

logger = logging.getLogger('ftrack-ace-delivery-action')


class DeliverAssetVersionsAction(object):
    '''This action runs on a list composed off renders. For each version, create deliverables according to the project requirements.'''

    label = 'DeliverAssetVersionsAction'
    identifier = 'deliverassetversionsaction.ace.net'
    description = 'Generates deliverables according to project.'

    def __init__(self, session, project_code):
        '''Initialise action.'''
        super(DeliverAssetVersionsAction, self).__init__()
        self.session = session
        # TODO: change per project
        self.project_code = project_code
        self.logger = logging.getLogger(
            __name__ + '.' + self.__class__.__name__
        )

    def register(self):
        '''Register action.'''
        self.session.event_hub.subscribe(
            'topic=ftrack.action.discover and source.user.username={0}'.format(
                self.session.api_user
            ),
            self.discover
        )

        self.session.event_hub.subscribe(
            'topic=ftrack.action.launch and data.actionIdentifier={0} and '
            'source.user.username={1}'.format(
                self.identifier,
                self.session.api_user
            ),
            self.launch
        )

    def discover(self, event):
        '''Return action config if triggered on a list of deliverables.'''
        logger.info("HEY HEY FROM ACTION")
        data = event['data']
        has_valid_asset_list = False

        selection = data.get('selection', [])

        if (
                len(selection) != 1 or
                selection[0]['entityType'] != 'list'
        ):
            return

        # TODO: Check if list is deliverable
        
        for entity in selection:
            print(entity.get("entityId"))
            current_list = self.session.get("List", entity.get("entityId"))
            if self.project_code != "default" and current_list['project']['name'] != self.project_code:
                continue
            self.project_id = current_list['project']['id']
            self.project_name = current_list['project']['name']

            for item in current_list['items']:
                if str(item.entity_type) == 'AssetVersion':
                    has_valid_asset_list = True
                    break

        if not has_valid_asset_list:
            return

        return {
            'items': [{
                'label': self.label,
                'description': self.description,
                'actionIdentifier': self.identifier
            }]
        }

    def launch(self, event):
        '''Callback method for custom action.'''
        selection = event['data'].get('selection', [])

        # Today
        today_date_str = datetime.today().strftime("%Y-%m-%d")

        fs_location = self.session.query('Location where name is "ftrack.unmanaged"').one()

        for entity in selection:
            if entity.get("entityType") == "list":
                current_list = self.session.get("List", entity.get("entityId"))
                logger.info(current_list['name'])
                for item in current_list['items']:
                    logger.info('Enity type:' + str(item.entity_type))
                    if str(item.entity_type) == 'AssetVersion':
                        logger.info('Running command' % item)
                        render_path = ""
                        for component in item['components']:
                            if component['name'] == 'Render (DPX)':
                                render_path = fs_location.get_filesystem_path(component)
                                logger.info(render_path)
                                logger.info(json.loads(component['metadata']['ftr_meta']).keys())
                                meta_data = json.loads(component['metadata']['ftr_meta'])
                                #subprocess.call(['python', r'/mnt/projects/potemkino/hfg_2019-11-20/outgoing/delivery/dev/code/generate_file.py', meta_data['path'], str(meta_data['first']), str(meta_data['last'])])
                                break

        return {
            'success': True,
            'message': 'Deliver action finished!'
        }


def register(session, **kw):
    '''Register plugin.

    Validate that session is an instance of ftrack_api.Session. If not, assume that register is being called from an incompatible API and return without doing anything.
    '''
    if not isinstance(session, ftrack_api.Session):
        return

    action = DeliverAssetVersionsAction(session, "hfg_2019-11-20")
    action.register()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    server_url='https://thefridge.ftrackapp.com'
    api_key='M2YxN2UyYWQtYzkyNS00NGJjLWIxZDMtNTc2NTdmZjM4MDc0OjoxOTQ4YjU2OS0wZDIwLTQ3ZGUtYjMyYy01OTc5MDc2NzcxMmM'
    api_user='tech@thefridge.tv'
    session = ftrack_api.Session()
    register(session)

    '''Wait for events.'''
    session.event_hub.wait()
