import logging
import socket
import functools

from QtExt import QtWidgets, QtCore

import ftrack_api
import ftrack_connect.util
import ftrack_connect.ui.application


logger = logging.getLogger(
    'multiple-instances-plugin'
)


def open_warning_dialog(message):
    '''Open warning window.'''
    parent = None
    for item in QtCore.QCoreApplication.instance().topLevelWidgets():
        if isinstance(item, ftrack_connect.ui.application.Application):
            parent = item

    dialog = QtWidgets.QMessageBox(parent=parent)
    dialog.setText(message)
    dialog.setIcon(QtWidgets.QMessageBox.Critical)
    dialog.exec_()


def handle_reply(event):
    '''Handle reply event if someone else is already running Connect.'''
    logger.warning('Someone else is already running connect: {0}'.format(event))
    ftrack_connect.util.invoke_in_main_thread(
        open_warning_dialog,
        u'ftrack Connect is already running on machine: {0}'.format(
            event['data']['machine']
        )
    )


def callback(event):
    '''Handle start of another connect.'''
    logger.warning('Someone just started ftrack Connect: {0}'.format(event))
    ftrack_connect.util.invoke_in_main_thread(
        functools.partial(
            open_warning_dialog,
            u'Connect just opened on machine: {0}'.format(
                event['data']['machine']
            )
        )
    )
    return {'machine': socket.gethostname()}


def register(session, **kw):
    '''Register hooks.'''
    logger.info(
        'Register detection of multiple Connect instances'
    )

    # Validate that session is an instance of ftrack_api.session.Session. If
    # not, assume that register is being called from an old or incompatible API
    # and return without doing anything.
    if not isinstance(session, ftrack_api.Session):
        logger.debug(
            'Not subscribing plugin as passed argument {0!r} is not an '
            'Session instance.'.format(session)
        )
        return

    session.event_hub.subscribe(
        'topic=studio.ftrack-connect.start and source.id != "{0}" and '
        'data.username = "{1}"'.format(
            session.event_hub.id, session._api_user
        ),
        callback
    )
    session.event_hub.publish(
        ftrack_api.event.base.Event(
            topic='studio.ftrack-connect.start',
            data={
                'username': session._api_user,
                'machine': socket.gethostname()
            }
        ),
        on_reply=handle_reply
    )